#/bin/bash

IMAGE=msts-public/gitlab-runner

VERSION=$(grep "^FROM" Dockerfile | awk -F: '{ print $2 }')
RELEASE=${RELEASE:-1}
VSTRING=${VERSION}.${RELEASE}

REGISTRY=registry.gitlab.com

build_args="--rm"

if [ ! -z ${SQUASH} ]
then
    build_args="--rm --squash"
fi

time docker build ${build_args} \
    -t ${IMAGE}:${VSTRING} \
    -t ${IMAGE}:latest \
    -t ${REGISTRY}/${IMAGE}:${VSTRING} \
    -t ${REGISTRY}/${IMAGE}:latest \
    .

if [ ! -z ${SQUASH} ]
then
    echo -e "\nPush to registry ${REGISTRY}: \n"
    echo -e "\tdocker login ${REGISTRY}"
    echo -e "\tdocker push ${REGISTRY}/${IMAGE}:${VSTRING}"
    echo -e "\tdocker push ${REGISTRY}/${IMAGE}:latest\n"
fi

exit 0
