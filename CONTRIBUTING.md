# Contribution Guidelines

## Contributor Process
1. Fork the repository
2. Clone your fork of the repository
3. Create a branch in your fork of the repository
4. Make the proposed changes in your branch
5. Commit changes
6. Push your branch
7. Create a merge request
8. Notice of pending merge will be publicized in Slack for comment/review

## Maintainer Process
1. Invite additional comment on the merge request where needed
2. Accept and merge where appropriate