FROM gitlab/gitlab-runner:alpine-v12.5.0

# GitLab Runner with AWS bootstrap tools

LABEL maintainer="cakirke@msts.com"

# Add tools to support 
ENV HOME_BIN /root/.local/bin
ENV PATH $HOME_BIN:$PATH
ENV PS1 "msts-public/gitlab-runner:\\w\$ "

ENV AWSCLI_VERSION  "1.16.282"

RUN apk add --update \
    build-base \
    python \
    python-dev \
    py-pip \
 && pip install awscli==${AWSCLI_VERSION} --upgrade --user \
 && apk del --purge -v \
    build-base \
    python-dev \
    py-pip \
 && rm -rf /var/cache/apk/*
