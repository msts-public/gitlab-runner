# MSTS GitLab Runner

## General
* Based on upstream [Alpine GitLab Runner](https://hub.docker.com/r/gitlab/gitlab-runner) image
* AWS CLI added to support bootsrapping builds
* Available in [GitLab Public Registry](registry.gitlab.com/msts-public/gitlab-runner) to eliminate authentication dependency for pull

# Building
* Only needed for debugging or if preparing to contribute

```
git clone git@gitlab.com:msts-public/gitlab-runner.git
cd gitlab-runner
./build.sh
```
## Maintaining
```
git clone git@gitlab.com:msts-public/gitlab-runner.git
cd gitlab-runner
# make changes
SQUASH=1 ./build.sh
# follow instructions to push
```
## Contributing
* See [CONTRIBUTING](Contributing.md) guidelines
